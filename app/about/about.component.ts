import { module,
    IComponentOptions } from 'angular';
import { ModuleName } from '../app';

let componentName = 'bmAbout';

class AboutComponentController {

    $routerOnActivate(next) { }
}

let aboutComponent: IComponentOptions = {
    templateUrl: 'app/about/about.component.html',
    controller: AboutComponentController
};

module(ModuleName).component(componentName, aboutComponent);

export const ComponentName = componentName;