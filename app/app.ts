import { bootstrap,
    module,
    element,
    ILocationProvider } from 'angular';
import '@angular/router/angular1/angular_1_router';

let appName = 'app';
export const ModuleName = appName;

module(appName, ['ngComponentRouter'])
    .config(($locationProvider: ILocationProvider) => {
        $locationProvider.html5Mode(true);
    })
    .value('$routerRootComponent', 'bmAppRouter');
element(document).ready(() => bootstrap(document, [appName]));

import './app-router/app-router.component';




