import { module,
    IComponentOptions } from 'angular';
import { ModuleName } from '../app';
import {ComponentName as productListComponentName} from '../product-list/product-list.component';
import {ComponentName as productComponentName} from '../product/product.component';
import {ComponentName as aboutComponentName} from '../about/about.component';

let componentName = 'bmAppRouter';

let appRouterComponent: IComponentOptions = {
    templateUrl: 'app/app-router/app-router.component.html',
    $routeConfig: [
        { path: '/products', name: 'Products', component: productListComponentName, useAsDefault: true },
        { path: '/product/:id', name: 'Product', component: productComponentName },
        { path: '/about', name: 'About', component: aboutComponentName }
    ]
};

module(ModuleName).component(componentName, appRouterComponent);

export const ComponentName = componentName;



