import { module,
    IQService,
    IPromise } from 'angular';
import { ModuleName } from '../app';
import { IProduct } from '../models';

let serviceName = 'productService';

export interface IProductService {
    getProducts(): IPromise<IProduct[]>;
}

class ProductService implements IProductService {

    static $inject = [
        '$q'
    ];

    constructor(private $q: IQService) { }

    getProducts(): IPromise<IProduct[]> {
        let products: IProduct[] = [
            { id: 1, name: 'Product 1' },
            { id: 2, name: 'Product 2' },
        ];
        return this.$q.when(products);
    }
}

module(ModuleName).service(serviceName, ProductService);

export const ServiceName = serviceName;

