import { module,
    IComponentOptions } from 'angular';
import { ModuleName } from '../app';

let componentName = 'bmProduct';

class ProductComponentController {

    private id: number;

    $routerOnActivate(next) {
        this.id = next.params.id;
    }
}

let productComponent: IComponentOptions = {
    templateUrl: 'app/product/product.component.html',
    controller: ProductComponentController,
    controllerAs: 'product'
};

module(ModuleName).component(componentName, productComponent);

export const ComponentName = componentName;