/**
 * System configuration for Angular 2 samples
 * Adjust as necessary for your application needs.
 */
(function (global) {
    // map tells the System loader where to look for things
    var map = {
        'app': 'app', // 'dist',
        'angular': 'node_modules/angular/angular.js',
        '@angular/router/angular1/angular_1_router': 'node_modules/@angular/router/angular1/angular_1_router.js'
    };

    var meta = {
        'node_modules/angular/angular.js': {
            format: 'global',
            exports: 'angular'
        },
        'node_modules/@angular/router/angular1/angular_1_router.js': {
            format: 'global'
            //scriptLoad: true
        }
    }

    // packages tells the System loader how to load when no filename and/or no extension
    var packages = {
        'app': { main: 'app.js', defaultExtension: 'js' }
    };

    var config = {
        map: map,
        meta: meta,
        packages: packages
    };
    System.config(config);
})(this);